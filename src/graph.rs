use std::collections::HashMap;
use thiserror::Error;

use crate::{
    edge::EdgeTrait,
    node::{BaseNode, NamedNodeTrait, NodeId},
};

pub type GraphResult<T> = Result<T, GraphError>;

#[derive(Debug, Error)]
pub enum GraphError {
    #[error("Node with this id already exists")]
    NodeExists,
    #[error("Node with name {0} does not exist")]
    NodeNameNotFound(String),
    #[error("Node with id {0} does not exist")]
    NodeIdNotFound(NodeId),
    #[error("Edge between nodes with the following ids ({0}, {1}) not found")]
    EdgeNotFoundByIds(NodeId, NodeId),
    #[error("Edge between nodes with the following ids ({0}, {1}) already exists")]
    EdgeExistsByIds(NodeId, NodeId),
}

#[derive(Debug, Default)]
pub struct Graph<N, E>
where
    N: BaseNode,
    E: EdgeTrait,
{
    nodes: HashMap<NodeId, N>,
    edges: HashMap<(NodeId, NodeId), E>,
}

impl<N, E> Graph<N, E>
where
    N: BaseNode,
    E: EdgeTrait,
{
    pub fn drop_node_by_id(&mut self, node_id: NodeId) -> GraphResult<()> {
        self.edges
            .retain(|&key, _| key.0 != node_id && key.1 != node_id);

        match self.nodes.remove(&node_id) {
            Some(_) => Ok(()),
            None => Err(GraphError::NodeIdNotFound(node_id)),
        }
    }

    pub fn add_edge_by_ids(
        &mut self,
        from_id: NodeId,
        to_id: NodeId,
        weight: E::Weight,
    ) -> GraphResult<()> {
        match self.edges.contains_key(&(from_id, to_id)) {
            true => Err(GraphError::EdgeExistsByIds(from_id, to_id)),
            false => {
                self.edges.insert((from_id, to_id), E::new(weight));
                Ok(())
            }
        }
    }

    pub fn drop_edge_by_ids(&mut self, from_id: NodeId, to_id: NodeId) -> GraphResult<()> {
        match self.edges.remove(&(from_id, to_id)) {
            Some(_) => Ok(()),
            None => Err(GraphError::EdgeNotFoundByIds(from_id, to_id)),
        }
    }

    pub fn node_by_id(&self, node_id: NodeId) -> Option<&N> {
        self.nodes.get(&node_id)
    }

    pub fn edge_by_ids(&self, from_id: NodeId, to_id: NodeId) -> Option<&E> {
        self.edges.get(&(from_id, to_id))
    }

    pub fn edge_mut_by_ids(&mut self, from_id: NodeId, to_id: NodeId) -> Option<&mut E> {
        self.edges.get_mut(&(from_id, to_id))
    }

    pub fn nodes_number(&self) -> usize {
        self.nodes.len()
    }

    pub fn edges_number(&self) -> usize {
        self.edges.len()
    }

    pub fn nodes(&self) -> Nodes<N> {
        Nodes {
            iter: self
                .nodes
                .iter()
                .map(|(_, v)| v)
                .collect::<Vec<_>>()
                .into_iter(),
        }
    }

    pub fn nodes_mut(&mut self) -> NodesMut<N> {
        NodesMut {
            iter: self
                .nodes
                .iter_mut()
                .map(|(_, v)| v)
                .collect::<Vec<_>>()
                .into_iter(),
        }
    }

    pub fn edges(&self) -> Edges<E> {
        Edges {
            iter: self.edges.iter(),
        }
    }

    pub fn edges_mut(&mut self) -> EdgesMut<E> {
        EdgesMut {
            iter: self.edges.iter_mut(),
        }
    }

    pub fn neighbors_by_id(&self, node_id: NodeId) -> Neighbors<N, E> {
        let iter = self
            .edges
            .iter()
            .filter(|(k, _)| k.0 == node_id)
            .map(|(k, v)| (&self.nodes[&k.1], v))
            .collect::<Vec<(&N, &E)>>()
            .into_iter();

        Neighbors { iter }
    }

    pub fn neighbors_mut_by_id(&mut self, node_id: NodeId) -> NeighborsMut<N, E> {
        let iter = self
            .edges
            .iter_mut()
            .filter(|(k, _)| k.0 == node_id)
            .map(|(k, v)| (&self.nodes[&k.1], v))
            .collect::<Vec<(&N, &mut E)>>()
            .into_iter();

        NeighborsMut { iter }
    }
}

impl<N, E> Graph<N, E>
where
    N: BaseNode + NamedNodeTrait,
    E: EdgeTrait,
{
    pub fn add_node(&mut self, node_name: &str) -> GraphResult<()> {
        if self.contains_node(node_name) {
            return Err(GraphError::NodeExists);
        }

        let mut node = N::new(self.nodes.len() as NodeId);
        node.set_name(node_name);

        self.nodes.insert(node.id(), node);

        Ok(())
    }

    pub fn contains_node(&self, name: &str) -> bool {
        self.get_node_id_by_name(name).is_some()
    }

    pub fn get_node_id_by_name(&self, node_name: &str) -> Option<NodeId> {
        self.nodes
            .iter()
            .find(|(_, v)| v.name() == node_name)
            .map(|(&id, _)| id)
    }

    pub fn drop_node(&mut self, node_name: &str) -> GraphResult<()> {
        let node_id = self
            .get_node_id_by_name(node_name)
            .ok_or_else(|| GraphError::NodeNameNotFound(node_name.to_string()))?;

        self.drop_node_by_id(node_id)
    }

    pub fn add_edge(&mut self, from: &str, to: &str, weight: E::Weight) -> GraphResult<()> {
        let (from_id, to_id) = self.get_node_ids_by_names(from, to)?;
        self.add_edge_by_ids(from_id, to_id, weight)
    }

    fn get_node_ids_by_names(&self, from: &str, to: &str) -> GraphResult<(NodeId, NodeId)> {
        let from_id = self
            .get_node_id_by_name(from)
            .ok_or_else(|| GraphError::NodeNameNotFound(from.to_string()))?;
        let to_id = self
            .get_node_id_by_name(to)
            .ok_or_else(|| GraphError::NodeNameNotFound(to.to_string()))?;

        Ok((from_id, to_id))
    }

    pub fn drop_edge(&mut self, from: &str, to: &str) -> GraphResult<()> {
        let (from_id, to_id) = self.get_node_ids_by_names(from, to)?;
        self.drop_edge_by_ids(from_id, to_id)
    }

    pub fn node(&self, node_name: &str) -> Option<&N> {
        match self.get_node_id_by_name(node_name) {
            Some(id) => self.node_by_id(id),
            None => None,
        }
    }

    pub fn edge(&self, from: &str, to: &str) -> Option<&E> {
        match self.get_node_ids_by_names(from, to) {
            Ok((from_id, to_id)) => self.edge_by_ids(from_id, to_id),
            Err(_) => None,
        }
    }

    pub fn edge_mut(&mut self, from: &str, to: &str) -> Option<&mut E> {
        match self.get_node_ids_by_names(from, to) {
            Ok((from_id, to_id)) => self.edge_mut_by_ids(from_id, to_id),
            Err(_) => None,
        }
    }

    pub fn neighbors_mut(&mut self, node_name: &str) -> NeighborsMut<N, E> {
        match self.get_node_id_by_name(node_name) {
            Some(node_id) => self.neighbors_mut_by_id(node_id),
            None => {
                let edges_number = self.edges.len();
                let iter = self
                    .edges
                    .iter_mut()
                    .skip(edges_number)
                    .map(|(k, v)| (&self.nodes[&k.1], v))
                    .collect::<Vec<(&N, &mut E)>>()
                    .into_iter();

                NeighborsMut { iter }
            }
        }
    }

    pub fn neighbors(&self, node_name: &str) -> Neighbors<N, E> {
        match self.get_node_id_by_name(node_name) {
            Some(node_id) => self.neighbors_by_id(node_id),
            None => {
                let edges_number = self.edges.len();
                let iter = self
                    .edges
                    .iter()
                    .skip(edges_number)
                    .map(|(k, v)| (&self.nodes[&k.1], v))
                    .collect::<Vec<(&N, &E)>>()
                    .into_iter();

                Neighbors { iter }
            }
        }
    }
}

#[derive(Debug)]
pub struct Neighbors<'a, N, E>
where
    N: BaseNode,
    E: EdgeTrait,
{
    iter: std::vec::IntoIter<(&'a N, &'a E)>,
}

impl<'a, N, E> Iterator for Neighbors<'a, N, E>
where
    N: BaseNode,
    E: EdgeTrait,
{
    type Item = (&'a N, &'a E);

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}

#[derive(Debug)]
pub struct NeighborsMut<'a, N, E>
where
    E: EdgeTrait,
{
    iter: std::vec::IntoIter<(&'a N, &'a mut E)>,
}

impl<'a, N, E> Iterator for NeighborsMut<'a, N, E>
where
    E: EdgeTrait,
{
    type Item = (&'a N, &'a mut E);

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}

#[derive(Debug)]
pub struct Nodes<'a, N>
where
    N: BaseNode,
{
    iter: std::vec::IntoIter<&'a N>,
}

impl<'a, N> Iterator for Nodes<'a, N>
where
    N: BaseNode,
{
    type Item = &'a N;

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}

#[derive(Debug)]
pub struct NodesMut<'a, N>
where
    N: BaseNode,
{
    iter: std::vec::IntoIter<&'a mut N>,
}

impl<'a, N> Iterator for NodesMut<'a, N>
where
    N: BaseNode,
{
    type Item = &'a mut N;

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}

#[derive(Debug)]
pub struct Edges<'a, E> {
    iter: std::collections::hash_map::Iter<'a, (NodeId, NodeId), E>,
}

impl<'a, E> Iterator for Edges<'a, E> {
    type Item = (&'a (NodeId, NodeId), &'a E);

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}

#[derive(Debug)]
pub struct EdgesMut<'a, E> {
    iter: std::collections::hash_map::IterMut<'a, (NodeId, NodeId), E>,
}

impl<'a, E> Iterator for EdgesMut<'a, E> {
    type Item = (&'a (NodeId, NodeId), &'a mut E);

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}
