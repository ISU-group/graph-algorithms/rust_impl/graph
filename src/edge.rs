pub trait EdgeTrait: Default {
    type Weight;

    fn new(weight: Self::Weight) -> Self;

    fn weight(&self) -> Self::Weight;
}
