use std::fmt::Debug;

pub type NodeId = usize;

pub trait BaseNode: Debug {
    fn new(id: NodeId) -> Self;

    fn id(&self) -> NodeId;
}

pub trait NamedNodeTrait: Debug {
    fn name(&self) -> &str;

    fn set_name(&mut self, name: &str);
}
