#[allow(unused_imports)]
pub use crate::graph::{Graph, GraphError, GraphResult};

pub mod edge;
pub mod graph;
pub mod node;

#[cfg(test)]
mod tests {
    use crate::edge::EdgeTrait;
    use crate::node::*;

    use super::*;

    #[derive(Debug, Default)]
    struct Edge {
        weight: f32,
    }

    impl edge::EdgeTrait for Edge {
        type Weight = f32;

        fn new(weight: Self::Weight) -> Self {
            Edge { weight }
        }

        fn weight(&self) -> Self::Weight {
            self.weight
        }
    }

    impl Edge {
        pub fn update(&mut self) {
            self.weight = 0.0;
        }
    }

    #[derive(Debug, Default)]
    struct Node {
        pub(crate) id: node::NodeId,
        pub(crate) name: String,
    }

    impl BaseNode for Node {
        fn new(id: node::NodeId) -> Self {
            Self {
                id,
                name: String::default(),
            }
        }

        fn id(&self) -> node::NodeId {
            self.id
        }
    }

    impl NamedNodeTrait for Node {
        fn name(&self) -> &str {
            &self.name
        }

        fn set_name(&mut self, name: &str) {
            self.name = name.to_string();
        }
    }

    #[test]
    fn default() {
        Graph::<Node, Edge>::default();
    }

    #[test]
    fn add_node() {
        let mut graph = Graph::<Node, Edge>::default();
        graph.add_node("A").unwrap();
        graph.add_node("B").unwrap();

        assert_eq!(graph.nodes_number(), 2);
    }

    #[test]
    fn add_edge() {
        let mut graph = Graph::<Node, Edge>::default();
        graph.add_node("A").unwrap();
        graph.add_node("B").unwrap();

        graph.add_edge("A", "B", 1.0).unwrap();

        assert_eq!(graph.edges_number(), 1);
    }

    #[test]
    fn drop_edge() {
        let mut graph = Graph::<Node, Edge>::default();
        graph.add_node("A").unwrap();
        graph.add_node("B").unwrap();

        graph.add_edge("A", "B", 1.0).unwrap();
        graph.drop_edge("A", "B").unwrap();

        assert_eq!(graph.nodes_number(), 2);
        assert_eq!(graph.edges_number(), 0);
    }

    #[test]
    fn drop_node() {
        let mut graph = Graph::<Node, Edge>::default();
        graph.add_node("A").unwrap();
        graph.add_node("B").unwrap();

        graph.add_edge("A", "B", 1.0).unwrap();

        assert_eq!(graph.nodes_number(), 2);
        assert_eq!(graph.edges_number(), 1);

        graph.drop_node("B").unwrap();

        println!("{graph:#?}");

        assert_eq!(graph.nodes_number(), 1);
        assert_eq!(graph.edges_number(), 0);
    }

    #[test]
    fn neighbors() {
        let mut graph = Graph::<Node, Edge>::default();
        graph.add_node("A").unwrap();
        graph.add_node("B").unwrap();

        graph.add_edge("A", "B", 1.0).unwrap();

        let mut neighbors = graph.neighbors("A");

        match neighbors.next() {
            Some((node, edge)) => {
                assert_eq!(node.name(), "B");
                assert_eq!(edge.weight(), 1.0);
            }
            None => panic!(),
        }

        assert!(neighbors.next().is_none());
    }

    #[test]
    fn neighbors_mut() {
        let mut graph = Graph::<Node, Edge>::default();
        graph.add_node("A").unwrap();
        graph.add_node("B").unwrap();

        graph.add_edge("A", "B", 1.0).unwrap();

        let mut neighbors = graph.neighbors_mut("A");

        match neighbors.next() {
            Some((node, edge)) => {
                assert_eq!(node.name(), "B");
                assert_eq!(edge.weight(), 1.0);

                edge.update();
            }
            None => panic!(),
        }

        assert!(neighbors.next().is_none());

        graph.add_node("C").unwrap();
    }

    #[test]
    fn edges() {
        let mut graph = Graph::<Node, Edge>::default();
        graph.add_node("A").unwrap();
        graph.add_node("B").unwrap();
        graph.add_node("C").unwrap();
        graph.add_node("D").unwrap();

        graph.add_edge("A", "B", 1.0).unwrap();
        graph.add_edge("A", "C", 2.0).unwrap();
        graph.add_edge("C", "B", 3.0).unwrap();
        graph.add_edge("D", "C", 2.0).unwrap();
        graph.add_edge("B", "C", 1.0).unwrap();

        assert_eq!(graph.edges().collect::<Vec<_>>().len(), 5);
    }

    #[test]
    fn edges_mut() {
        let mut graph = Graph::<Node, Edge>::default();
        graph.add_node("A").unwrap();
        graph.add_node("B").unwrap();
        graph.add_node("C").unwrap();
        graph.add_node("D").unwrap();

        graph.add_edge("A", "B", 1.0).unwrap();
        graph.add_edge("A", "C", 2.0).unwrap();
        graph.add_edge("C", "B", 3.0).unwrap();
        graph.add_edge("D", "C", 2.0).unwrap();
        graph.add_edge("B", "C", 1.0).unwrap();

        let mut edges = graph.edges_mut();

        for (_, edge) in &mut edges {
            edge.weight = 0.0;
        }

        graph.edges().for_each(|(_, e)| assert_eq!(e.weight(), 0.0));
    }
}
